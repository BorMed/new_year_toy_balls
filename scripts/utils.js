const path = require('path');
const fs = require('fs');

function cleanToyBallsStyles() {
    const emptyToyBallsStyles = `/**
 * Styles of toy balls for including in a style-tag. They are created during the prod build
 */
export const toyBallsStyles: string  = '';
`;

    fs.writeFileSync(
        path.resolve(__dirname, '..', 'src', 'toy-balls-styles.ts'),
        emptyToyBallsStyles
    );
}

module.exports.cleanToyBallsStyles = cleanToyBallsStyles;

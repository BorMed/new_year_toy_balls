const path = require('path');
const chPr = require('child_process');
const fsExtra = require('fs-extra');
const fs = require('fs');
const cleanToyBallsStyles = require('./utils').cleanToyBallsStyles;

const buildCss = 'npm run buildScss';
const buildDev = 'npm run dev';
const buildProd = 'npm run buildMin';

chPr.execSync(buildCss, {stdio: "inherit"});

cleanToyBallsStyles();

const pathToIntermediateResult = path.resolve(__dirname, '..', '_intermediate-result');
const pathToMinifiedToToyBallsStyles = path.join(
    pathToIntermediateResult,
    'toyballs-browserlist.min.css'
);
const pathToToyBallsStyles = path.resolve(
    __dirname,
    '..',
    'src',
    'toy-balls-styles.ts'
    );

chPr.execSync(buildDev);

const minCss = fs.readFileSync(pathToMinifiedToToyBallsStyles).toString();
const newToyBallsStyles = `export const toyBallsStyles: string  = '${minCss}'`;
fs.writeFileSync(pathToToyBallsStyles, newToyBallsStyles);

chPr.execSync(buildProd);

const pathToDist = path.resolve(__dirname, '..', 'dist');
//Create the dist directory
fsExtra.ensureDirSync(pathToDist);
fsExtra.emptyDirSync(pathToDist);
fsExtra.copySync(
    path.join(pathToIntermediateResult, 'toyballs.min.js'),
    path.join(pathToDist, 'toyballs.min.js')
);
fsExtra.copySync(
    path.join(pathToIntermediateResult, 'toyballs-browserlist.min.css'),
    path.join(pathToDist, 'toyballs.min.css')
);
fsExtra.copySync(
    path.join(pathToIntermediateResult, 'toyballs.js'),
    path.join(pathToDist, 'toyballs.js')
);

cleanToyBallsStyles();

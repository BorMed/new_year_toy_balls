const path = require('path');

module.exports = {
    entry: [
        path.resolve(__dirname, 'src', 'toy-balls.ts'),
    ],
    output: {
        path: path.resolve(__dirname, '_intermediate-result'),
        filename: 'toyballs.min.js'
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/
            },
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    }
};

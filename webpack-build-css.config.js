const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
    entry: [
        path.resolve(__dirname, 'src', 'scss', 'scss.ts'),
    ],
    output: {
        path: path.resolve(__dirname, '_intermediate-result'),
        filename: 'scss.js'
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /src/,
                include: /scss/
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader"
                    },
                    {
                        loader: 'postcss-loader',
                    },
                    "sass-loader"
                ]
            },

        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js', '.css', '.scss']
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "toyballs-browserlist.min.css",
        }),
        new OptimizeCSSAssetsPlugin({}),
    ],
};

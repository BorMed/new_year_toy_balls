export class Utils {
    public static createDiv(...classes: string[]): HTMLDivElement {
        const result = document.createElement('div');
        if (classes) {
            result.classList.add(...classes);
        }
        return result;
    }
}

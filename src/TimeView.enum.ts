export enum TimeView {
    DAY = 'day',
    HOUR = 'hour',
    MINUTE = 'minute',
    SECOND = 'second'
}

export function getTimeViewDescription(type: TimeView) {
    if (type === TimeView.DAY) {
        return 'days'
    } else if (type === TimeView.HOUR) {
        return 'hours'
    } else if (type === TimeView.MINUTE) {
        return 'minutes'
    } else if (type === TimeView.SECOND) {
        return 'seconds'
    }
}

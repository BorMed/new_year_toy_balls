import { TimeView } from './TimeView.enum';

export interface Options {
    container: HTMLElement;
    targetDate?: Date;
    onFinalCb?: () => boolean;
    enableAnimation?: boolean;
    // HEX
    ballColors?: {
        first: string;
        firstReflect: string;
        firstShadow: string;
        second: string;
        secondReflect: string;
        secondShadow: string;
        third: string;
        thirdReflect: string;
        thirdShadow: string;
        fourth: string;
        fourthReflect: string;
        fourthShadow: string;
    },
    background?: {
        baseR: string;
        baseG: string;
        baseB: string;
        top?: string;
    }
}

export interface BallDomElement {
    timeType: TimeView;
    fullToy: HTMLElement;
    ball: HTMLElement;
    time: HTMLElement;
}

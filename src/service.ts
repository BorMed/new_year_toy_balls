import { ToyBalls } from "./toy-balls";
import { CountDown } from "./countDown";

export class Service {
    toyBalls: ToyBalls;
    countDown: CountDown;
    orderMovingBallsShortAnimation: number[] = [500, 400, 100, 300];

    constructor() {
    }

    runShortAnimation() {
        if (this.toyBalls.isEnabledAnimation) {
            // @ts-ignore
            (window as any).NewYearToyBalls.balls.forEach((b, index) => {
                setTimeout(
                    () => b.fullToy.children[0].classList.add('toy-balls-container__full-toy--short-spin-animation'),
                    this.orderMovingBallsShortAnimation[index]
                );
                setTimeout(() =>
                        b.fullToy.children[0].classList.remove('toy-balls-container__full-toy--short-spin-animation'),
                    1000 + this.orderMovingBallsShortAnimation[index]
                );
            });
        }
    }

    createDiv(classes: string[]): HTMLDivElement {
        const result = document.createElement('div');
        result.classList.add(...classes);
        return result;
    }
}

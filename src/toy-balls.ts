import { toyBallsStyles } from "./toy-balls-styles";
import { CountDown } from "./countDown";
import { getTimeViewDescription, TimeView } from "./TimeView.enum";
import { Utils } from "./utils";
import { Service } from "./service";
import { spruceBranch } from "./spruce-branch";
import { BallDomElement, Options } from './model';

export class ToyBalls {
    public static readonly MAIN_NODE_CLASS_NAME: string = 'toy-balls-main-node';
    public static readonly BALL_CLASS_NAME: string = 'toy-balls-container__ball';
    public static readonly HEAD_CLASS_NAME: string = 'toy-balls-container__head';
    public static readonly HEAD_TEXT_CLASS_NAME: string = 'toy-balls-container__head-text';
    public static readonly FULL_TOY_CLASS_NAME: string = 'toy-balls-container__full-toy';
    public static readonly TOY_THREAD_CLASS_NAME: string = 'toy-balls-container__toy-thread';
    public static readonly FULL_TOY_WRAPPER_CLASS_NAME: string = 'toy-balls-container__full-toy-wrapper';
    public static readonly TIME_DESCRIPTION_CLASS_NAME: string = 'toy-balls-container__time-description';
    public static readonly ALL_TOYS_CONTAINER_CLASS_NAME: string = 'toy-balls-container';
    public static readonly ALL_TOYS_CONTAINER_WRAPPER_CLASS_NAME: string = 'toy-balls-container-wrapper';

    public isEnabledAnimation: Boolean;
    public balls: BallDomElement[] = [];
    public currentOpts: Options;

    private readonly service: Service;
    private countDown: CountDown;
    private mainNode: HTMLElement = null;
    private dayView: BallDomElement = null;
    private hourView: BallDomElement = null;
    private minuteView: BallDomElement = null;
    private secondView: BallDomElement = null;
    private ballsContainer: HTMLElement = null;
    private ballsContainerWrapper: HTMLDivElement = null;
    private passedOpts: Options;

    public constructor() {
        this.service = new Service();
        this.service.toyBalls = this;
        this.currentOpts = this.initialCurrentOpts()
    }

    private initialCurrentOpts(): Options {
        const nextYear = new Date().getFullYear() + 1;
        console.log('FULL ', nextYear);
        return {
            targetDate: new Date(nextYear.toString()),
            enableAnimation: true,
            onFinalCb: () => true,
            container: null,
        }
    }

    /**
     * Create ball-toys and put them into the pointed node
     *
     * @param incomingOpts {@link Options}
     */
    public create(incomingOpts: Options): void {
        this.createStyles();
        this.passedOpts = incomingOpts;
        this.processOptions(incomingOpts);
        this.createCountDown();
        this.initBallsContainer();
        this.createBalls();
        this.putBallsToContainer();
        this.bindBallsToTime();
        this.createHeadText();
        this.setCssVariables();
        this.createSpruceBranch()
    }

    public enableAnimation() {
        this.isEnabledAnimation = true;
    }

    public disableAnimation(): void {
        this.isEnabledAnimation = false;
    }

    /**
     * Create styles and put them into the DOM by a style tag
     */
    private createStyles(): void {
        if (!document.head.querySelector('.toy-balls-styles')) {
            const stylesElem: HTMLStyleElement = document.createElement('style');
            stylesElem.classList.add('toy-balls-styles');
            stylesElem.innerText = toyBallsStyles;
            document.head.append(stylesElem);
        }
    }

    private createToy(type: TimeView): BallDomElement {
        const time: HTMLDivElement = Utils.createDiv();

        const ball: HTMLDivElement
            = Utils.createDiv(ToyBalls.BALL_CLASS_NAME, ToyBalls.BALL_CLASS_NAME + '--' + type);
        ball.append(time);

        const timeDescription = Utils.createDiv(ToyBalls.TIME_DESCRIPTION_CLASS_NAME);
        timeDescription.innerText = getTimeViewDescription(type);

        let thread: HTMLDivElement = Utils.createDiv(ToyBalls.TOY_THREAD_CLASS_NAME);
        if (type === TimeView.DAY) {
            thread.classList.add(ToyBalls.TOY_THREAD_CLASS_NAME + '--first');
        } else if (type === TimeView.HOUR) {
            thread.classList.add(ToyBalls.TOY_THREAD_CLASS_NAME + '--second')
        } else if (type === TimeView.MINUTE) {
            thread.classList.add(ToyBalls.TOY_THREAD_CLASS_NAME + '--third')
        } else if (type === TimeView.SECOND) {
            thread.classList.add(ToyBalls.TOY_THREAD_CLASS_NAME + '--fourth')
        }

        const fullToy: HTMLDivElement = Utils.createDiv(ToyBalls.FULL_TOY_CLASS_NAME);
        fullToy.append(thread);
        fullToy.append(ball);
        fullToy.addEventListener('mouseover', () => {
            if (this.isEnabledAnimation && !fullToy.classList.contains('toy-balls-container__full-toy--spin-animation')) {
                fullToy.classList.add('toy-balls-container__full-toy--spin-animation');
                setTimeout(() => {
                    fullToy.classList.remove('toy-balls-container__full-toy--spin-animation')
                }, 1500);
            }
        });

        if (type === TimeView.DAY || type === TimeView.MINUTE) {
            fullToy.classList.add(ToyBalls.FULL_TOY_CLASS_NAME + '--incline-left')
        } else {
            fullToy.classList.add(ToyBalls.FULL_TOY_CLASS_NAME + '--incline-right')
        }


        const fullToyWrapper: HTMLElement = Utils.createDiv(ToyBalls.FULL_TOY_WRAPPER_CLASS_NAME);
        fullToyWrapper.append(fullToy);
        fullToyWrapper.append(timeDescription);

        return { fullToy: fullToyWrapper, ball, timeType: type, time };
    }

    private initBallsContainer(): void {
        this.ballsContainerWrapper = Utils.createDiv(ToyBalls.ALL_TOYS_CONTAINER_WRAPPER_CLASS_NAME);
        this.mainNode.append(this.ballsContainerWrapper);

        this.ballsContainer = Utils.createDiv(ToyBalls.ALL_TOYS_CONTAINER_CLASS_NAME);
        this.ballsContainerWrapper.append(this.ballsContainer);
    }

    private putBallsToContainer(): void {
        this.balls.forEach((b) => this.ballsContainer.append(b.fullToy));
    }

    private createBalls(): void {
        Object.keys(TimeView).forEach((key: any) => {
                const name = key.toLowerCase();
                (this as any)[name + 'View'] = this.createToy((name as any));
                this.balls.push((this as any)[name + 'View']);
            }
        );
    }

    private bindBallsToTime(): void {
        this.countDown.bindDay(this.dayView.time);
        this.countDown.bindHour(this.hourView.time);
        this.countDown.bindMinute(this.minuteView.time);
        this.countDown.bindSecond(this.secondView.time);
        this.countDown.bindResolve();
    }

    private createHeadText() {
        let head = Utils.createDiv(ToyBalls.HEAD_CLASS_NAME);
        let text = Utils.createDiv(ToyBalls.HEAD_TEXT_CLASS_NAME);
        head.append(text);
        text.innerText = `Until the New ${this.currentOpts.targetDate.getFullYear()} left`;
        this.ballsContainerWrapper.prepend(head);
    }

    private setCssVariables() {
        this.ballsContainerWrapper.style.setProperty(
            '--toy-balls-base-size',
            '' + this.mainNode.offsetWidth + 'px'
        );
        this.ballsContainerWrapper.style.setProperty(
            '--toy-balls-base-size-mines',
            '-' + this.mainNode.offsetWidth + 'px'
        );
        if (this.passedOpts.background) {
            this.mainNode.style.setProperty(
                '--background-color-base-r',
                this.passedOpts.background.baseR
            );
            this.mainNode.style.setProperty(
                '--background-color-base-g',
                this.passedOpts.background.baseG
            );
            this.mainNode.style.setProperty(
                '--background-color-base-b',
                this.passedOpts.background.baseB
            );
            if (this.passedOpts.background.top) {
                this.mainNode.style.setProperty(
                    '--background-color-top',
                    this.passedOpts.background.top
                );
            }
        }
    }

    private processOptions(opts: Options) {
        if (!opts.container) {
            throw new Error('A passed container of toy balls is undefined');
        }
        this.mainNode = opts.container;
        // @ts-ignore
        Object.keys(opts)
            // @ts-ignore
            .filter(k => opts[k] !== undefined)
            // @ts-ignore
            .forEach(k => this.currentOpts[k] = opts[k]);
        this.isEnabledAnimation = this.currentOpts.enableAnimation;
        this.mainNode.classList.add(ToyBalls.MAIN_NODE_CLASS_NAME);
    }

    private createCountDown() {
        this.countDown = new CountDown(this, this.service);
    }

    private createSpruceBranch() {
        let svg: SVGElement = document.createElementNS(
            "http://www.w3.org/2000/svg",
            'svg'
        );
        svg.setAttribute('viewBox', `0 0 504 207`);
        svg.setAttribute('fill', 'url(#spruce-branch-gradient)');
        svg.setAttribute('stroke', 'black');
        svg.setAttribute('stroke-width', '4px');
        svg.style.width = this.mainNode.offsetWidth + 'px';
        svg.innerHTML = spruceBranch;

        const div = Utils.createDiv('spruce-branch-wrapper');
        div.append(svg);
        this.ballsContainerWrapper.prepend(div);
    }
}

if (!(window as any).NewYearToyBalls) {
    (window as any).NewYearToyBalls = new ToyBalls();
}

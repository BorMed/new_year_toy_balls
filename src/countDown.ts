import {ToyBalls} from "./toy-balls";
import { Service } from "./service";

export class CountDown {
    public static readonly TIME_CLASS_NAME: string = 'count-down-time';
    public static readonly TIME_WRAPPER_CLASS_NAME: string = 'count-down-time-wrapper';
    private readonly finalLogic: () => void;

    /**
     * Called when a binding of a time to a view is completed
     */
    public bindResolve: Function;
    /**
     * The remaining time in milliseconds
     */
    private allRestTime: number;
    /**
     * the remaining time in days
     */
    private restDays: number;
    /**
     * The remaining time until the end of the day in hours
     */
    private restHours: number;
    /**
     * The remaining time until the end of the hour in minutes
     */
    private restMinutes: number;
    /**
     * The remaining time until the end of the minute in seconds
     */
    private restSeconds: number;
    private targetDate: Date;
    private countDownTimerRef: any;
    private dayView: HTMLDivElement;
    private hourView: HTMLDivElement;
    private minuteView: HTMLDivElement;
    private secondView: HTMLDivElement;
    /**
     * A promise that is resolved when a binding of a time to a view was passed
     */
    private bound: Promise<any>;
    private toyBalls: ToyBalls;
    private service: Service;

    public constructor(toyBalls: ToyBalls, service: Service) {
        this.toyBalls = toyBalls;
        this.service = service;
        this.service.countDown = this;
        this.targetDate = toyBalls.currentOpts.targetDate;
        this.finalLogic = toyBalls.currentOpts.onFinalCb;
        this.initViews();

        const now = new Date();
        this.calcAllRestTime(now);
        this.calcRestSeconds(now);
        this.calcRestMinutesHoursDays(now, this.allRestTime);

        this.initCountDawn();
    }

    public stopCountDown(): void {
        clearInterval(this.countDownTimerRef);
    }

    public bindDay(div: HTMLElement): void {
        div.classList.add(CountDown.TIME_WRAPPER_CLASS_NAME, 'animated');

        this.dayView = document.createElement('div');
        this.dayView.classList.add(
            CountDown.TIME_CLASS_NAME,
            CountDown.TIME_CLASS_NAME + '--days'
        );
        div.append(this.dayView);
    }

    public bindHour(div: HTMLElement): void {
        div.classList.add(CountDown.TIME_WRAPPER_CLASS_NAME, 'animated');

        this.hourView = document.createElement('div');
        this.hourView.classList.add(
            CountDown.TIME_CLASS_NAME,
            CountDown.TIME_CLASS_NAME + '--hours'
        );
        div.append(this.hourView);
    }

    public bindMinute(div: HTMLElement): void {
        div.classList.add(CountDown.TIME_WRAPPER_CLASS_NAME, 'animated');

        this.minuteView = document.createElement('div');
        this.minuteView.classList.add(
            CountDown.TIME_CLASS_NAME,
            CountDown.TIME_CLASS_NAME + '--minutes'
        );
        div.append(this.minuteView);
    }

    public bindSecond(div: HTMLElement): void {
        div.classList.add(CountDown.TIME_WRAPPER_CLASS_NAME, 'animated');

        this.secondView = document.createElement('div');
        this.secondView.classList.add(
            CountDown.TIME_CLASS_NAME,
            CountDown.TIME_CLASS_NAME + '--seconds'
        );
        div.append(this.secondView);
    }

    private initCountDawn(): void {
        this.countDownTimerRef = setInterval(() => {
            const nowDate = new Date();
            this.calcAllRestTime(nowDate);
            this.calcRestSeconds(nowDate);

            this.updateView(this.secondView, this.restSeconds);

            if (nowDate.getSeconds() === 0) {
                this.calcRestMinutesHoursDays(nowDate, this.allRestTime);
                this.updateViewMinutesHoursDays();
            }

            if (this.allRestTime < 0) {
                clearInterval(this.countDownTimerRef);
                this.runFinalLogic();
            }
        }, 1000);
    }

    private initViews(): void {
        this.bound = new Promise<any>((resolve, reject) => {
            this.bindResolve = resolve;
        });
        this.bound.then(() => {
            this.updateView(this.secondView, this.restSeconds);
            this.updateViewMinutesHoursDays();
        });
    }

    private updateView(div: HTMLDivElement, newValue: number): void {
        if (this.toyBalls.isEnabledAnimation) {
            div.parentElement.classList.remove('fadeIn');
            div.parentElement.classList.add('fadeOut');
        }
        setTimeout(() => {
            div.innerText = newValue + '';
            if (this.toyBalls.isEnabledAnimation) {
                div.parentElement.classList.add('fadeIn');
                div.parentElement.classList.remove('fadeOut');
            }
        }, 500);

    }

    private calcRestMinutesHoursDays(nowDate: Date, allRestTime: number): void {

        this.restHours = 23 - nowDate.getHours();
        this.restMinutes = 59 - nowDate.getMinutes();
        this.restDays = Math.floor(allRestTime / (86400000));
    }

    private calcRestSeconds(nowDate: Date): void {
        this.restSeconds = 59 - nowDate.getSeconds();
    }

    private calcAllRestTime(nowDate: Date): void {
        this.allRestTime = this.targetDate.getTime() - nowDate.getTime();
    }

    private runFinalLogic(): void {
        this.finalLogic();
    }

    private updateViewMinutesHoursDays(): void {
        this.updateView(this.minuteView, this.restMinutes);
        this.updateView(this.dayView, this.restDays);
        this.updateView(this.hourView, this.restHours);
        this.service.runShortAnimation();
    }
}

const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
    entry: [
        path.resolve(__dirname, 'src', 'main.ts'),
    ],
    output: {
        sourceMapFilename: "./toyballs.js.map",
        pathinfo: true,
        path: path.resolve(__dirname, '_intermediate-result'),
        filename: 'toyballs.js'
    },
    devtool: "inline-source-map",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader"
                    },
                    "sass-loader",
                ]
            },
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js', '.css', '.scss']
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "toyballs.min.css",
        }),
        new OptimizeCSSAssetsPlugin({}),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'src', 'index.html'),
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, '_intermediate-result'),
        overlay: true,
        compress: false,
        open: true,
        port: 9001
    },
};
